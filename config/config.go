package config

import (
	"encoding/json"
	"fmt"
	"os"
)

type Config struct {
	Database struct {
		Dialect  string `json:"db_dialect"`
		Username string `json:"db_username"`
		Password string `json:"db_password"`
		Name     string `json:"db_name"`
		Charset  string `json:"db_charset"`
		Host     string `json:"db_host"`
		Sslmode  string `json:"db_sslmode"`
	} `json:"database"`

	Mtn struct {
		MomoPayUrl string `json:"momo_pay_url"`
	} `json:"mtn"`

	Tigo struct {
		TigoCashUrl string `json:"tigo_cash_url"`
	} `json:"tigo"`
}

func LoadConfiguration(file string) Config {
	var config Config
	configFile, err := os.Open(file)
	defer configFile.Close()
	if err != nil {
		fmt.Println(err.Error())
	}
	jsonParser := json.NewDecoder(configFile)
	jsonParser.Decode(&config)
	return config
}

func GetConfig() Config {
	config := LoadConfiguration("config.json")
	// fmt.Println("MomoPayUrl >>> ", config.Mtn.MomoPayUrl)
	// fmt.Println("TigoCashUrl >>> ", config.Tigo.TigoCashUrl)
	return config
}
