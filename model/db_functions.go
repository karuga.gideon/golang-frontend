package model

import (
	"bytes"
	"database/sql"
	"fmt"
	"log"
	"strings"
	"time"

	"github.com/jinzhu/gorm"
	"github.com/vjeantet/jodaTime"
)

var dbURI = "root:root@/esd?charset=utf8&parseTime=True"
var mysqlConnection = "root:root@tcp(127.0.0.1:3306)/esd"
var dbDialect = "mysql"

func checkError(err error) {
	if err != nil {
		log.Println(err)
		return
	}
}

// GetInvoiceCount - gets the total number of all the invoices in db
func GetInvoiceCount() uint64 {
	var invoiceCount uint64
	db, err := gorm.Open(dbDialect, dbURI)
	if err != nil {
		log.Fatal("Could not connect database")
	}
	db.Table("invoices").Count(&invoiceCount)
	return invoiceCount
}

// GetInvoiceCountByBranch - gets the total number of all the invoices in db by branch
func GetInvoiceCountByBranch(branchName string) uint64 {
	var invoiceCount uint64
	db, err := gorm.Open(dbDialect, dbURI)
	if err != nil {
		log.Fatal("Could not connect database")
	}
	db.Table("invoices").Where("branch = ?", branchName).Count(&invoiceCount)
	return invoiceCount
}

// GetTodaysInvoiceCount - get the count of invoices signed today
func GetTodaysInvoiceCount() uint64 {
	var invoiceCount uint64
	db, err := gorm.Open(dbDialect, dbURI)
	if err != nil {
		log.Fatal("Could not connect database")
	}
	date := jodaTime.Format("YYYY-MM-dd", time.Now())
	// fmt.Println(date)
	db.Where("date_created LIKE ?", "%"+date+"%").Table("invoices").Count(&invoiceCount)
	return invoiceCount
}

// GetUserCount - gets the total number of users in the db
func GetUserCount() uint64 {
	var userCount uint64
	db, err := gorm.Open(dbDialect, dbURI)
	if err != nil {
		log.Fatal("Could not connect database")
	}
	db.Table("users").Count(&userCount)
	return userCount
}

// GetVatSum -
func GetVatSum() string {
	var invoiceCount string
	// Connect to database
	db, err := sql.Open(dbDialect, mysqlConnection)
	checkError(err)

	// Get data from db
	results, err := db.Query("SELECT sum(vat) FROM invoices ")
	checkError(err)

	for results.Next() {
		var id string
		err = results.Scan(&id)
		checkError(err)
		fmt.Println("Invoice vat sum func >>> ", id)
		invoiceCount = id

	}
	defer results.Close()
	return invoiceCount
}

// GetGrandTotalSum -
func GetGrandTotalSum() string {
	var invoiceCount string
	// Connect to database
	db, err := sql.Open(dbDialect, mysqlConnection)
	checkError(err)

	// Get data from db
	results, err := db.Query("SELECT sum(grand_total) FROM invoices ")
	checkError(err)

	for results.Next() {
		var id string
		err = results.Scan(&id)
		checkError(err)
		// fmt.Println("Invoice count func >>> ", id)
		invoiceCount = id
	}

	defer results.Close()
	return invoiceCount
}

// GetAllInvoices -
func GetAllInvoices() uint64 {

	var invoiceCount uint64

	// Connect to database
	db, err := sql.Open(dbDialect, mysqlConnection)
	checkError(err)

	// Get data from db
	results, err := db.Query("SELECT * FROM invoices ")
	checkError(err)

	for results.Next() {
		var id uint64
		err = results.Scan(&id)
		checkError(err)
		// fmt.Println("Invoice count func >>> ", id)
		invoiceCount = id
	}

	defer results.Close()
	return invoiceCount
}

// GetInvoices - retrieves invoices from db using pagination
func GetInvoices(offSet, pageNum, pageSize int) interface{} {
	db, err := gorm.Open(dbDialect, dbURI)
	if err != nil {
		log.Fatal("Could not connect database")
	}
	invoices := []Invoice{}
	db.Offset(offSet).Limit(pageSize).Order("Id DESC").Find(&invoices)
	return invoices
}

// GetInvoicesByBranch - retrieves invoices from db using pagination
func GetInvoicesByBranch(branchName string, offSet, pageNum, pageSize int) interface{} {
	db, err := gorm.Open(dbDialect, dbURI)
	if err != nil {
		log.Fatal("Could not connect database")
	}
	invoices := []Invoice{}
	// Get all matched records
	db.Where("branch = ?", branchName).Offset(offSet).Limit(pageSize).Order("Id DESC").Find(&invoices)
	return invoices
}

// SearchInvoice - retrieves invoices from db using pagination
func SearchInvoice(query string) interface{} {
	db, err := gorm.Open(dbDialect, dbURI)
	if err != nil {
		log.Fatal("Could not connect database")
	}
	invoices := []Invoice{}
	db.Where("invoice_number LIKE ?", "%"+query+"%").Order("Id DESC").Find(&invoices)
	return invoices
}

// GetInvoice Gets single invoice
func GetInvoice(id int) *Invoice {
	// fmt.Println("Getting invoice by id >>> ", id)
	invoice := getInvoiceOr404(id)
	return invoice
}

// getInvoiceOr404 gets a invoice instance if exists, or respond the 404 error otherwise
func getInvoiceOr404(invoiceID int) *Invoice {
	invoice := Invoice{}
	db, err := gorm.Open(dbDialect, dbURI)
	if err != nil {
		log.Fatal("Could not connect database")
	}

	if err := db.First(&invoice, Invoice{Id: invoiceID}).Error; err != nil {
		// respondError(w, http.StatusNotFound, err.Error())
		return nil
	}
	return &invoice
}

// ValidateUser - validate a user during portal login
func ValidateUser(username string, password string) *User {
	userValidation := User{}
	db, err := gorm.Open(dbDialect, dbURI)
	if err != nil {
		log.Fatal("Could not connect database")
	}
	db.Where("email = ? AND password = ?", username, password).Find(&userValidation)
	return &userValidation
}

// GetUsers - retrieve users from the db
func GetUsers(offSet, pageNum, pageSize int) interface{} {

	db, err := gorm.Open(dbDialect, dbURI)
	if err != nil {
		log.Fatal("Could not connect database")
	}

	users := []User{}
	db.Offset(offSet).Limit(pageSize).Order("Id DESC").Find(&users)
	// fmt.Println("users >>> ", users)
	return users
}

// GetUser Gets single user from db
func GetUser(id int) *User {
	// fmt.Println("Getting invoice by id >>> ", id)
	user := getUserOr404(id)
	return user
}

// CreateUser - creates a new user
func CreateUser(user User) int {
	db, err := gorm.Open(dbDialect, dbURI)
	if err != nil {
		log.Fatal("Could not connect database")
	}

	if err := db.Save(&user).Error; err != nil {
		log.Fatal("Could not create user", err)
		return 0
	}
	return 1
}

// getInvoiceOr404 gets a invoice instance if exists, or respond the 404 error otherwise
func getUserOr404(userID int) *User {
	user := User{}
	db, err := gorm.Open(dbDialect, dbURI)
	if err != nil {
		log.Fatal("Could not connect database")
	}

	if err := db.First(&user, User{Id: userID}).Error; err != nil {
		// respondError(w, http.StatusNotFound, err.Error())
		return nil
	}
	return &user
}

// NumberToString - converts number to string
func NumberToString(n uint64, sep rune) string {

	// s := strconv.Itoa(n)
	s := fmt.Sprintf("%v", n)
	startOffset := 0
	var buff bytes.Buffer

	if n < 0 {
		startOffset = 1
		buff.WriteByte('-')
	}

	l := len(s)

	commaIndex := 3 - ((l - startOffset) % 3)

	if commaIndex == 3 {
		commaIndex = 0
	}

	for i := startOffset; i < l; i++ {

		if commaIndex == 3 {
			buff.WriteRune(sep)
			commaIndex = 0
		}
		commaIndex++

		buff.WriteByte(s[i])
	}

	return buff.String()
}

// FormatNumberString - converts number to string
func FormatNumberString(s string, sep rune) string {

	startOffset := 0
	var buff bytes.Buffer

	if len(s) < 0 {
		startOffset = 1
		buff.WriteByte('-')
	}

	l := len(s)

	commaIndex := 3 - ((l - startOffset) % 3)

	if commaIndex == 3 {
		commaIndex = 0
	}

	for i := startOffset; i < l; i++ {

		if commaIndex == 3 {
			buff.WriteRune(sep)
			commaIndex = 0
		}
		commaIndex++

		buff.WriteByte(s[i])
	}

	str := strings.Replace(buff.String(), ",.", ".", -1)

	return str
}

// gorm url
// http://doc.gorm.io/advanced.html#compose-primary-key
