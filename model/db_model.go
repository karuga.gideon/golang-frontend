package model

import (
	"time"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

// User - users table / model
type User struct {
	Id             int       `json:"id"`
	Email          string    `json:"email"`
	Firstname      string    `json:"firstname"`
	Lastname       string    `json:"lastname"`
	Msisdn         string    `json:"msisdn"`
	Password       string    `json:"password"`
	Apikey         string    `json:"apikey"`
	ExpiryTime     string    `json:"expiry_time"`
	CreationTime   time.Time `json:"creation_time"`
	InTrash        string    `json:"in_trash"`
	ValidationCode string    `json:"validation_code"`
}

// Invoice table / model
type Invoice struct {
	Id            int       `json:"id"`
	DateCreated   time.Time `json:"date_created"`
	InvoiceDate   string    `json:"invoice_date"`
	InvoiceNumber string    `json:"invoice_number"`
	Vat           string    `json:"vat"`
	GrandTotal    string    `json:"grand_total"`
	Signature     string    `json:"signature"`
	Branch        string    `json:"branch"`
}

type PageData struct {
	Title              string
	FirstName          string
	Message            string
	Error              string
	InvoiceCount       string
	TodaysInvoiceCount string
	InvoiceVat         string
	InvoiceGrandTotal  string
	Invoices           interface{}
	Invoice            interface{}
	Users              interface{}
	User               interface{}
	NumPages           int
	RowCount           uint64
	Pagination         interface{}
	Pages              interface{}
}

// Page - sort page numbers on front end
type Page struct {
	PageNumber int
}

// DBMigrate will create and migrate the tables, and then make the some relationships if necessary
func DBMigrate(db *gorm.DB) *gorm.DB {
	// db.AutoMigrate(&Organization{}, &User{}, &Transaction{})
	// db.Model(&User{}).AddForeignKey("org_id", "organizations(id)", "CASCADE", "CASCADE")
	return db
}
