package main

import (
	"bytes"
	"fmt"
	"html/template"
	"log"
	"math"
	"net/http"
	"reflect"
	"strconv"
	"strings"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/securecookie"
	"github.com/tealeg/xlsx"

	"esd-golang-frontend/config"
	model "esd-golang-frontend/model"
)

var tpl *template.Template
var admin *template.Template
var app *template.Template
var forms *template.Template
var view *template.Template

var mysqlConnection = ""
var dbDialect = ""

// to control reports page size
const pageSize = 10

// cookie handling
var cookieHandler = securecookie.New(
	securecookie.GenerateRandomKey(64),
	securecookie.GenerateRandomKey(32))

func init() {
	tpl = template.Must(template.ParseGlob("./templates/*.gohtml"))
	admin = template.Must(template.ParseGlob("./templates/admin/*.gohtml"))
	app = template.Must(template.ParseGlob("./templates/admin/app/*.gohtml"))
	forms = template.Must(template.ParseGlob("./templates/admin/forms/*.gohtml"))
	view = template.Must(template.ParseGlob("./templates/admin/view/*.gohtml"))
}

func main() {

	http.HandleFunc("/", index)
	http.HandleFunc("/index", index)
	http.HandleFunc("/about", about)
	http.HandleFunc("/contact", contact)
	http.HandleFunc("/login", login)
	http.HandleFunc("/bootstrap", bootstrap)

	// Admin login
	http.HandleFunc("/dashboard", dashboard)
	http.HandleFunc("/invoices/", invoices)
	http.HandleFunc("/branch_invoices/", branchInvoices)
	http.HandleFunc("/invoice/", invoice)
	http.HandleFunc("/search_invoice", searchInvoice)
	http.HandleFunc("/export_invoices", exportInvoicesToExcel)
	http.HandleFunc("/users/", users)
	http.HandleFunc("/user/", user)
	http.HandleFunc("/add_user", addUser)
	http.HandleFunc("/profile", profile)

	http.Handle("/assets/", http.StripPrefix("/assets/", http.FileServer(http.Dir("assets"))))

	config := config.GetConfig()
	// var mysqlConnection = "root:root@tcp(127.0.0.1:3306)/cocoon"
	mysqlConnection = config.Database.Username + ":" + config.Database.Password + "@tcp(127.0.0.1:3306)/" + config.Database.Name
	log.Println("Db dialect : ", config.Database.Dialect)

	log.Println("Application started on port : 2054")
	log.Println("v1.0.5")
	http.ListenAndServe(":2054", nil)
}

func checkError(w http.ResponseWriter, err error) {
	if err != nil {
		log.Println(err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}
}

func index(w http.ResponseWriter, req *http.Request) {
	pgData := model.PageData{
		Title: "Home",
	}
	err := tpl.ExecuteTemplate(w, "index.gohtml", pgData)
	checkError(w, err)
}

func about(w http.ResponseWriter, req *http.Request) {
	pgData := model.PageData{
		Title: "About",
	}
	err := tpl.ExecuteTemplate(w, "about.gohtml", pgData)
	checkError(w, err)
}

func contact(w http.ResponseWriter, req *http.Request) {
	pgData := model.PageData{
		Title: "Contact",
	}
	err := tpl.ExecuteTemplate(w, "contact.gohtml", pgData)
	checkError(w, err)
}

func bootstrap(w http.ResponseWriter, req *http.Request) {
	pgData := model.PageData{
		Title: "Bootstrap",
	}
	err := tpl.ExecuteTemplate(w, "bootstrap.gohtml", pgData)
	checkError(w, err)
}

func login(w http.ResponseWriter, req *http.Request) {

	pgData := model.PageData{
		Title: "Login",
	}

	var username string

	if req.Method == http.MethodPost {
		username = req.FormValue("username")
		password := req.FormValue("password")
		user := model.ValidateUser(username, password)
		if user.Id > 0 {
			// .. check credentials ..
			pgData.FirstName = username
			setSession(username, w)
			redirectTarget := "/dashboard"
			http.Redirect(w, req, redirectTarget, 302)
		} else {
			pgData.Error = "Invalid login."
			deleteCookie(w)
			err := tpl.ExecuteTemplate(w, "login.gohtml", pgData)
			checkError(w, err)
		}

	} else {
		deleteCookie(w)
		err := tpl.ExecuteTemplate(w, "login.gohtml", pgData)
		checkError(w, err)
	}
}

func setSession(userName string, response http.ResponseWriter) {
	value := map[string]string{
		"name": userName,
	}
	if encoded, err := cookieHandler.Encode("session", value); err == nil {
		cookie := &http.Cookie{
			Name:  "session",
			Value: encoded,
			Path:  "/",
		}
		http.SetCookie(response, cookie)
	}
}

// deleteCookie on user sign out
func deleteCookie(response http.ResponseWriter) {
	cookie := &http.Cookie{
		Name:   "session",
		Value:  "",
		Path:   "/",
		MaxAge: -1,
	}
	http.SetCookie(response, cookie)
}

func testCookie(w http.ResponseWriter, r *http.Request) {
	cookie, _ := r.Cookie("session")
	// fmt.Println("session cookie >>> ", cookie)
	if cookie == nil {
		http.Redirect(w, r, "/login", 302)
	}
}

// Admin menus
func dashboard(w http.ResponseWriter, req *http.Request) {
	testCookie(w, req)
	pgData := model.PageData{
		Title:              "Dashboard",
		TodaysInvoiceCount: model.NumberToString(model.GetTodaysInvoiceCount(), ','),
		InvoiceCount:       model.NumberToString(model.GetInvoiceCount(), ','),
		InvoiceVat:         model.FormatNumberString(model.GetVatSum(), ','),
		InvoiceGrandTotal:  model.FormatNumberString(model.GetGrandTotalSum(), ','),
		Message:            "Welcome to ESD Reports. ",
	}
	err := app.ExecuteTemplate(w, "dashboard.gohtml", pgData)
	checkError(w, err)
}

func invoice(w http.ResponseWriter, req *http.Request) {
	testCookie(w, req)
	id := strings.TrimPrefix(req.URL.Path, "/invoice/")
	invoiceID, err := strconv.Atoi(id)
	if err != nil {
		fmt.Println("Error converting invoice id to int >>> ", err)
	}
	invoice := model.GetInvoice(invoiceID)
	pgData := model.PageData{
		Title:   "Invoice",
		Invoice: invoice,
	}
	err = view.ExecuteTemplate(w, "invoice.gohtml", pgData)
	checkError(w, err)
}

func searchInvoice(w http.ResponseWriter, req *http.Request) {
	testCookie(w, req)

	if req.Method == http.MethodPost {

		query := req.FormValue("query")

		pgData := model.PageData{
			Title:    "Invoice Search",
			Invoices: model.SearchInvoice(query),
		}
		err := admin.ExecuteTemplate(w, "invoices.gohtml", pgData)
		checkError(w, err)
	}
}

func profile(w http.ResponseWriter, req *http.Request) {
	testCookie(w, req)
	pgData := model.PageData{
		Title: "Profile",
	}
	err := view.ExecuteTemplate(w, "profile.gohtml", pgData)
	checkError(w, err)
}

func generatePagination(len int) string {
	var buffer bytes.Buffer

	buffer.WriteString(`	<div>
			<ul class="pagination">
				<li class="page-item disabled">
					<a class="page-link" href="#">&laquo;</a>
				</li>`)

	for i := 1; i <= len; i++ {
		if i == 1 {
			buffer.WriteString(`<li class="page-item active">
				<a class="page-link" href="#">` + strconv.Itoa(i) + `</a>
			</li>`)
		} else {
			buffer.WriteString(`<li class="page-item">
				<a class="page-link" href="#">` + strconv.Itoa(i) + `</a>
			</li>`)
		}
	}

	buffer.WriteString(`<li class="page-item">
			<a class="page-link" href="#">&raquo;</a>
		</li>
	</ul>
	</div>`)

	return buffer.String()
}

func generatePaginationStrut(len int) interface{} {
	var pages = []model.Page{}
	for i := 1; i <= len; i++ {
		pg := model.Page{
			PageNumber: i,
		}
		pages = append(pages, pg)
	}
	return pages
}

func invoices(w http.ResponseWriter, req *http.Request) {

	testCookie(w, req)

	pageNumStr := strings.TrimPrefix(req.URL.Path, "/invoices/")
	if pageNumStr == "" {
		pageNumStr = "1"
	}
	pageNum, err := strconv.Atoi(pageNumStr)

	checkError(w, err)

	var offSet int

	switch pageNum {
	case 0:
		pageNum = 1
		offSet = 0
	case 1:
		offSet = 0
	default:
		offSet = (pageNum - 1) * pageSize

	}

	invoiceCount := model.GetInvoiceCount()
	d := float64(invoiceCount) / float64(pageSize)
	numPages := int(math.Ceil(d))
	if numPages == 0 {
		numPages = 1
	}

	pgData := model.PageData{
		Title:      "Invoices",
		RowCount:   invoiceCount,
		NumPages:   numPages,
		Invoices:   model.GetInvoices(offSet, pageNum, pageSize),
		Pagination: generatePaginationStrut(numPages),
	}
	err = app.ExecuteTemplate(w, "invoices.gohtml", pgData)
	checkError(w, err)
}

func branchInvoices(w http.ResponseWriter, req *http.Request) {

	testCookie(w, req)

	branchDetails := strings.TrimPrefix(req.URL.Path, "/branch_invoices/")
	log.Println("branchDetails >>> ", branchDetails)

	branchName := branchDetails
	pageNumStr := "1"

	if strings.Contains(branchDetails, "/") {
		s := strings.Split(branchDetails, "/")
		branchName, pageNumStr := s[0], s[1]
		fmt.Println("branchName >> "+branchName, "pageNumStr >>> "+pageNumStr)
	}

	log.Println("Retrieving invoices for Branch >>> ", branchName)
	pageNum, err := strconv.Atoi(pageNumStr)

	checkError(w, err)

	var offSet int

	switch pageNum {
	case 0:
		pageNum = 1
		offSet = 0
	case 1:
		offSet = 0
	default:
		offSet = (pageNum - 1) * pageSize

	}

	invoiceCount := model.GetInvoiceCountByBranch(branchName)
	d := float64(invoiceCount) / float64(pageSize)
	numPages := int(math.Ceil(d))
	if numPages == 0 {
		numPages = 1
	}

	pgData := model.PageData{
		Title:      branchName + " Invoices",
		RowCount:   invoiceCount,
		NumPages:   numPages,
		Invoices:   model.GetInvoicesByBranch(branchName, offSet, pageNum, pageSize),
		Pagination: generatePaginationStrut(numPages),
	}
	err = app.ExecuteTemplate(w, "branch_invoices.gohtml", pgData)
	checkError(w, err)
}

func users(w http.ResponseWriter, req *http.Request) {
	testCookie(w, req)

	pageNumStr := strings.TrimPrefix(req.URL.Path, "/users/")
	if pageNumStr == "" {
		pageNumStr = "1"
	}
	pageNum, err := strconv.Atoi(pageNumStr)

	var offSet int

	switch pageNum {
	case 0:
		pageNum = 1
		offSet = 0
	case 1:
		offSet = 0
	default:
		offSet = (pageNum - 1) * pageSize

	}

	userCount := model.GetUserCount()
	d := float64(userCount) / float64(pageSize)
	numPages := int(math.Ceil(d))
	if numPages == 0 {
		numPages = 1
	}

	pgData := model.PageData{
		Title:      "Users",
		RowCount:   userCount,
		NumPages:   numPages,
		Users:      model.GetUsers(offSet, pageNum, pageSize),
		Pagination: generatePaginationStrut(numPages),
	}
	err = app.ExecuteTemplate(w, "users.gohtml", pgData)
	checkError(w, err)
}

func user(w http.ResponseWriter, req *http.Request) {
	testCookie(w, req)
	id := strings.TrimPrefix(req.URL.Path, "/user/")
	// fmt.Println("Getting user with id >>>> ", id)
	userID, err := strconv.Atoi(id)
	if err != nil {
		fmt.Println("Error converting user id to int >>> ", err)
	}
	user := model.GetUser(userID)
	// fmt.Println("Found user >>> ", user)
	pgData := model.PageData{
		Title: "User",
		User:  user,
	}
	err = view.ExecuteTemplate(w, "user.gohtml", pgData)
	checkError(w, err)
}

func addUser(w http.ResponseWriter, req *http.Request) {
	testCookie(w, req)
	pgData := model.PageData{
		Title: "Add User",
	}

	if req.Method == http.MethodPost {

		user := model.User{}

		firstname := req.FormValue("firstname")
		user.Firstname = firstname
		lastname := req.FormValue("lastname")
		user.Lastname = lastname
		msisdn := req.FormValue("msisdn")
		user.Msisdn = msisdn
		email := req.FormValue("email")
		user.Email = email
		password := req.FormValue("password")
		user.Password = password
		repeatPassword := req.FormValue("repeat_password")
		user.InTrash = "NO"

		// redirectTarget := "/users"

		if password != repeatPassword {
			// redirectTarget := "/add_user"
			pgData.Error = "Passwords do not match! Try again."
			err := forms.ExecuteTemplate(w, "add_user.gohtml", pgData)
			checkError(w, err)

		} else {

			userCreation := model.CreateUser(user)
			log.Println("userCreation result >>> ", userCreation)

			// if username == "info@esd.com" && password == "esadmin123#" {
			if userCreation > 0 {
				// .. check if saved successfully ..
				pgData.Message = "User added successfully."
				// redirectTarget := "/users"
				err := admin.ExecuteTemplate(w, "add_user.gohtml", pgData)
				checkError(w, err)
			} else {
				pgData.Error = "Error adding user."
				// redirectTarget := "/add_user"
				err := admin.ExecuteTemplate(w, "add_user.gohtml", pgData)
				checkError(w, err)
			}
		}

	} else {
		err := admin.ExecuteTemplate(w, "add_user.gohtml", pgData)
		checkError(w, err)
	}
}

func exportInvoicesToExcel(w http.ResponseWriter, req *http.Request) {

	var file *xlsx.File
	var sheet *xlsx.Sheet
	var row *xlsx.Row
	var cell *xlsx.Cell
	var err error

	file = xlsx.NewFile()
	sheet, err = file.AddSheet("Sheet1")
	if err != nil {
		fmt.Printf(err.Error())
	}

	// Header row
	row = sheet.AddRow()
	cell = row.AddCell()
	cell.Value = "ID"
	cell = row.AddCell()
	cell.Value = "Date Created"
	cell = row.AddCell()
	cell.Value = "Invoice Number"
	cell = row.AddCell()
	cell.Value = "Vat"
	cell = row.AddCell()
	cell.Value = "Grand Total"
	cell = row.AddCell()
	cell.Value = "Signature"

	invoices := model.GetInvoices(0, 1, 1000)

	// listSlice, ok := invoices.([]*model.Invoice)
	// fmt.Println("invoices >>> ", invoices)
	v := reflect.ValueOf(invoices)

	for i := 0; i < v.Len(); i++ {

		strct := v.Index(i).Interface()
		// fmt.Println(strct)
		x := reflect.ValueOf(strct)
		// fmt.Println(x.Field(2))

		// add rows to excel
		row = sheet.AddRow()
		idCell := row.AddCell()
		idCell.Value = strconv.Itoa(x.Field(0).Interface().(int))
		dateCreatedCell := row.AddCell()
		dateCreatedCell.Value = x.Field(1).Interface().(time.Time).String()
		invoiceNumberCell := row.AddCell()
		invoiceNumberCell.Value = x.Field(3).Interface().(string)
		cell = row.AddCell()
		cell.Value = x.Field(4).Interface().(string)
		cell = row.AddCell()
		cell.Value = x.Field(5).Interface().(string)
		cell = row.AddCell()
		cell.Value = x.Field(6).Interface().(string)

	}

	err = file.Save("./assets/exports/Invoices.xlsx")
	if err != nil {
		fmt.Printf(err.Error())
	}

	pgData := model.PageData{
		Title: "Invoices Export",
	}
	err = admin.ExecuteTemplate(w, "invoices_export.gohtml", pgData)
	checkError(w, err)
}
